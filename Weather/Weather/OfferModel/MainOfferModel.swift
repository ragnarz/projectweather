//
//  MainOfferModel.swift
//  Weather
//
//  Created by Михаил Копейкин on 08.07.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class MainOfferModel: Codable {
    
    var temp: Float?
    var temp_min:Float?
    var temp_max:Float?
    
}
