//
//  MainView.swift
//  Weather
//
//  Created by Михаил Копейкин on 28.07.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class MainView:  UIView {
    
    var tableView = UITableView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.firtInitialization()
        self.setupConstrints()
    }
    //регистрация ячейки
 fileprivate    func firtInitialization() {
    self.addSubview(tableView)
    
    self.tableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomTableViewCell")
    
    }
    
    fileprivate func setupConstrints() {
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        
        self.tableView.leftAnchor.constraint(equalTo:self.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo:self.rightAnchor).isActive = true
        self.tableView.topAnchor.constraint(equalTo:self.topAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo:self.bottomAnchor).isActive = true
        self.tableView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
           self.tableView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
}
