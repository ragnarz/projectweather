//
//  ViewController.swift
//  Weather
//
//  Created by Михаил Копейкин on 08.07.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UISearchResultsUpdating,UITableViewDataSource,UITableViewDelegate {
    
    
    var timer = Timer()
    

    fileprivate var contentView:MainView {
        return self.view as! MainView
    }
    
    
    //получение данных
    var dataIsReadi:Bool = false
    var offerModelAttributs:[CustomTableViewCellAttributs]! {
        didSet{
            DispatchQueue.main.async {
                self.contentView.tableView.reloadData()
            }
        }
    }
    
    var offerModel:OfferModel!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNavigationBar()
        (self.view as! MainView).tableView.dataSource = self
         (self.view as! MainView).tableView.delegate = self
    }
    
    
    
    
    
    //настройка навигации
    fileprivate func setupNavigationBar() {
        self.navigationItem.title = "Weahter Aplication"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        let searchController = UISearchController(searchResultsController: nil)
        
        searchController.searchResultsUpdater = self
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    //MARK:- UISearchResultsUpdating Called when the search bar's text or scope has changed or when the search bar becomes first responder.
    func updateSearchResults(for searchController: UISearchController){
        
        let city = searchController.searchBar.text!
        timer.invalidate()
        
        if city.count > 1 {
            timer = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { (timer) in
                NetworkManager.sharet.getWeather(city: city, result: { (model) in
                    if model != nil {
                        self.dataIsReadi = true
                        self.offerModel = model!
                        self.createOfferModelAttributs(model: model!)
                        
                    }
                })
            })
        }
        
        
    }
    private func createOfferModelAttributs(model:OfferModel){
        var _offerModelAttributs = [CustomTableViewCellAttributs]()
        let city = model.city!.name!
        
        for element in model.list! {
            _offerModelAttributs.append(CustomTableViewCellAttributs(cityName: city,
                                                                     time: element.dt_txt!,
                                                                    tempMinLable:element.main!.temp_min!.description,
                                                                    temp: element.main!.temp!.description,
                                                                    tempMax: element.main!.temp_max!.description))
        }
        self.offerModelAttributs = _offerModelAttributs
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    //MARK:- UITableVIewDataSoure
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataIsReadi {
            return self.offerModel!.list!.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell") as! CustomTableViewCell
        cell.setup(attr: self.offerModelAttributs[indexPath.row])

        
        
        
        return cell
    }
    
}

