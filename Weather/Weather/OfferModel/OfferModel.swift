//
//  OfferModel.swift
//  Weather
//
//  Created by Михаил Копейкин on 08.07.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class OfferModel: Codable {
    
    var cod : String?
    var message: Float?
    var cnt : Float?
    var list:[ListOfferModel]?
    var city:CityModel?
}
