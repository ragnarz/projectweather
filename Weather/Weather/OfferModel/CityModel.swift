//
//  CityModel.swift
//  Weather
//
//  Created by Михаил Копейкин on 28.07.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class CityModel:Codable {
    
    var id:Float?
    var name:String?
    var country:String?
    var population:Float?
}
