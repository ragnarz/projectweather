//
//  ListOfferModel.swift
//  Weather
//
//  Created by Михаил Копейкин on 08.07.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class ListOfferModel: Codable {
    var dt: Float?
    var main:MainOfferModel?
    var dt_txt:String?
}
