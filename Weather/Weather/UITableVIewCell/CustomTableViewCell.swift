//
//  CustomTableViewCell.swift
//  Weather
//
//  Created by Михаил Копейкин on 03.08.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class CustomTableViewCell:UITableViewCell{
    
    @IBOutlet weak var cityLable: UILabel!
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var tempMinLable: UILabel!
    @IBOutlet weak var tempLable: UILabel!
    @IBOutlet weak var tempMaxLable: UILabel!
    
    
    
    
    override var reuseIdentifier: String? {
        return "CustomTableViewCell"
    }
    
    func setup(attr:CustomTableViewCellAttributs){
        self.cityLable.text = attr.cityName
        self.timeLable.text = attr.time
        self.tempMinLable.text = attr.tempMinLable
        self.tempMaxLable.text = attr.tempMax
        self.tempLable.text = attr.temp
    }
}

struct CustomTableViewCellAttributs {
    let cityName:String
    let time:String
    let tempMinLable:String
    let temp:String
    let tempMax:String
}
